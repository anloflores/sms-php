import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SMS } from '@ionic-native/sms';
import { Sim } from '@ionic-native/sim';
import { HTTP } from '@ionic-native/http';
import { BackgroundMode } from '@ionic-native/background-mode';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  drafts = [];
  ids = [];
  removed = [];
  looper = null;
  logs = "";


  constructor(public navCtrl: NavController, public sms: SMS, public sim: Sim, private http: HTTP,private backgroundMode: BackgroundMode) {
    
    this.backgroundMode.setDefaults({
      title   : 'Complexus SMSGateWay',
      hidden  : true,
      silent  : false,
      text    : 'CompleXus GateWay For Schools'
    });

    this.backgroundMode.enable();

    this.sim.getSimInfo().then(
      (info) => {
        this.logger(Object.keys(info))
      },
      (err) => console.log('Unable to get sim info: ', err)
    );
    
    this.logger("Constructed");

    setInterval(() => {
      this.logger("Checking For New Message");
      this.check();
    }, 5000);

    this.looper = setInterval(() => {
      this.sender();
    }, 10000);

  }

  logger(msg) {
    msg += "\n";
    this.logs += msg;
  }

  sender() {
    var option = {
      replaceLineBreaks : true,
      android : {
        intent : ""
      }
    };

    if(this.ids.length != 0) {
      for(var d of this.drafts) {
        if(this.ids.indexOf(d.id) >= 0) {
          if(this.sms.send(d.number, d.message + "-----" + d.id, option)) {
            this.logger("Message Sent on number " + d.number);

            this.update(d.id);

            this.logger("Loop Pause");
            clearInterval(this.looper);

            this.logger("Eliminate Process");
            var index = this.ids.indexOf(d.id);
            this.ids.splice(index, 1);
            this.removed.push(d.id);

            
          } else {
            this.logger("Sending Error on number " + d.number);
          }
        }
      }
    }
  }

  check() {
    this.http.get('http://localhost:8000/?action=get', {}, {})
      .then(data => {

        var d = JSON.parse(data.data);
        if(d.status) {
          for(var g of d.data) {
            if(this.ids.indexOf(g.id) < 0 && this.removed[this.removed.indexOf(g.id)] == null) {
              this.drafts.push(g);
              this.ids.push(g.id);
              this.logger("New Message From API");
            }
          }
        }

      })
      .catch(error => {

        console.log(error.status);

      });
  }

  update(id) {
   
    this.http.get('http://localhost:8000/?action=update&id=' + id, {}, {})
      .then(data => {
         // delete on object
        this.logger("Current : " + id + " Remaining : " + this.ids);
        this.logger("---------------------------");
        this.logger("Loop Continued");
        this.looper = setInterval(() => {
          this.sender();
        }, 10000);
      })
      .catch(error => {

        console.log(error.status);

      });
  }

}
