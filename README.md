# SMS + PHP
This was made for a client. The Goal of this app is to use the your mobile phone as an API Gateway.

## Why is this created?
One of the requirement of the client is to have a SMS Attendance Alert, the user is roughly at around 7,000+ it'll be to expensive in the long run, since the web application will be offered free, the client doesn't want to avail an expensive SMS API. That's the sole reason that I created this app.

## How does it work?
- The Web Application will send a request to the API (the PHP File) then the API will store the data sent.
- API has a method that will return the messsages
- The Mobile App will send the response by batch in order to avoid the Courier Text Message Sending Limit
